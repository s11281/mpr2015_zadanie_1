package pl.pjwstk.edu.pl.s11281;

import java.util.ArrayList;
import java.util.List;

public class Role {

	private String name;
	private List<User> users;
	private List<Permission> permissions;
	
	public Role()
	{
		users = new ArrayList<User>();
		permissions=new ArrayList<Permission>();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	public List<Permission> getPermission() {
		return permissions;
	}

	public void setPermission(List<Permission> permissions) {
		this.permissions = permissions;
	}
	
	
	
}
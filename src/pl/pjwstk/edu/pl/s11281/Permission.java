package pl.pjwstk.edu.pl.s11281;

import java.util.ArrayList;
import java.util.List;

public class Permission {
	
	
	private String name;
	private List<Role> roles;

	public Permission()
	{
		roles = new ArrayList<Role>();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Role> getRoles() {
		return roles;
	}

	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}
	
	
}

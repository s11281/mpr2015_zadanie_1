package pl.pjwstk.edu.pl.s11281;

import java.util.ArrayList;
import java.util.List;

public class Person {
	
	private User user;
	private String firstName;
	private String lastName;
	private String phoneNumer;
	private List<Address> addresses;
	private List<Role> roles;
	
	public Person()
	{
		this.roles = new ArrayList<Role>();
		this.addresses=new ArrayList<Address>();
	}
	
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPhoneNumer() {
		return phoneNumer;
	}

	public void setPhoneNumer(String phoneNumer) {
		this.phoneNumer = phoneNumer;
	}

	public List<Address> getAddresses() {
		return addresses;
	}

	public void setAddresses(List<Address> addresses) {
		this.addresses = addresses;
	}
	public List<Role> getRoles() {
		return roles;
	}

	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}
	
	
}